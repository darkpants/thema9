# Classification Of Birds Using Machine Learning

---

Data scientists may want to examine the underlying relationship between sizes of the bones and ecological groups, and recognising a birds ecological group by their bonestructure. 
The goal of this research is creating a Java command-line application that predicts a birds ecological group based on the bone sizes with high accuracy.
This could be very usefull for scientists working with unidentified bird skeletons, for example argeologists, and will speed up the research progress.

---

## Important files

---

* Log.pdf* 
	- detailed instructions on how I did the exploratory data analysis.
* Hugo_Donkerbroek_Final_Report.pdf* 
	- final report on the project.

---

## Other files

---

- datafiles folder
	* contains all datafiles used in this project.

- img folder
	* contains the images used in the final report.

- school assignments
	* log_peer_review.txt
		- peer review I wrote for a fellow student.
	* ResultsDiscussion.PDF*
		- mid project results and discussion segment on the exploratory data analysis.

- other
	* reference.bib
	* vancouver.csl
		- both files used for references in the final report.

** For the Java command-line application go to [this](https://bitbucket.org/darkpants/javawrapper/src/master/) repository.**

---

*these files also have a .rmd version, which is the R markdown file
